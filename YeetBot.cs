﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

using Bot.Core;
using Bot.Core.Models;
using Bot.Core.Services;

namespace Bot {
    public class YeetBot : Globals {

        public IServiceProvider Providers;

        private readonly string RedditRssPattern = "<br/>\\s<span><a\\shref=\"(.*?)\">\\[link\\]<";
        private readonly string DiscordToken = GetTokenFromFile();

        private int ResetCount;

        private static string GetTokenFromFile() {
            var token = string.Empty;
            var fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "token.txt");

            try {
                if (!File.Exists(fileName))
                    File.Create(fileName);

                token = File.ReadAllText(fileName);
            }
            catch (Exception e) {
                Console.Error.WriteLine(e.StackTrace);
            }
            ConsoleMessage.Debug(fileName);
            return token;
        }

        public void Cooldown() {
            while (true) {
                InsultCooldownTimer--;
                if (InsultCooldownTimer <= 0) {
                    InsultCooldownTimer = 0;
                }
                if (InsultCooldownTimer >= 10) {
                    InsultCooldownTimer = 10;
                }
                //ConsoleMessage.Warning($"Insult cooldown set to: {InsultCooldownTimer}");
                Thread.Sleep(2000);
            }
        }

        /// <summary>
        /// Why do I care attempting to "better" this. I just want it to work. idc if smelly
        /// </summary>
        public void InsultCooldown() {
            while (true) {
                InsultCooldownTimer--;
                if (InsultCooldownTimer <= 0) {
                    InsultCooldownTimer = 0;
                }
                if (InsultCooldownTimer >= 10) {
                    InsultCooldownTimer = 10;
                }
                //ConsoleMessage.Warning($"Insult cooldown set to: {InsultCooldownTimer}");
                Thread.Sleep(2000);
            }
        }
        /// <summary>
        /// Why do I care attempting to "better" this. I just want it to work. idc if smelly
        /// </summary>
        public void OsrsCooldown() {
            while (true) {
                OsrsCooldownTimer--;
                if (OsrsCooldownTimer <= 0) {
                    OsrsCooldownTimer = 0;
                }
                if (OsrsCooldownTimer >= 10) {
                    OsrsCooldownTimer = 10;
                }
                //ConsoleMessage.Warning($"Osrs stats cooldown set to: {OsrsCooldownTimer}");
                Thread.Sleep(2000);
            }
        }

        public async Task RunBotAsync() {
            if (DiscordToken == string.Empty) {
                ConsoleMessage.Error("Your discord token isn't applied");
                Console.ReadLine();
                return;
            }

            DiscordClient = new DiscordSocketClient(new DiscordSocketConfig {
                LogLevel = LogSeverity.Debug
            });

            DiscordCommand = new CommandService(new CommandServiceConfig {
                LogLevel = LogSeverity.Debug,
                CaseSensitiveCommands = true,
                DefaultRunMode = RunMode.Async
            });

            Providers = new ServiceCollection()
                .AddSingleton(this)
                .AddSingleton(DiscordClient)
                .AddSingleton(DiscordCommand)
                .AddSingleton<AudioServices>()
                .AddSingleton<WebScraperServices>()
                .AddSingleton<InsultServices>()
                .BuildServiceProvider();

            await DiscordCommand.AddModulesAsync(Assembly.GetEntryAssembly(), Providers);
            await DiscordClient.LoginAsync(TokenType.Bot, DiscordToken);
            await DiscordClient.StartAsync();

            DiscordClient.MessageReceived += HandleCommandsAsync;
            DiscordClient.Log += ClientLogging;
            DiscordClient.Ready += ClientReady;
            DiscordClient.Connected += ClientConnected;
            DiscordClient.Disconnected += ClientDisconnected;
            DiscordClient.ReactionAdded += ClientOnReaction;

            WebScraper = new WebScraperServices();
            Insulter = new InsultServices();

            CensoredList = new List<CensoredUsers>();
            AdminFullList = new List<AdminUsers>();
            SilencedUserList = new List<Disabled>();
            FilterList = new List<Filter>();

            UrlContainer = new List<string>() {
                Capacity = 500
            };

            // Wait at least 5 seconds
            await Task.Delay(5000);
            // Do not await this. Messes up Cooldown

            if (ResetCount < 1) {
                InsultCooldownTask = Task.Factory.StartNew(InsultCooldown);
                OsrsCooldownTask = Task.Factory.StartNew(OsrsCooldown);
                RedditThread = new Thread(CrawlReddit);
                RedditThread.Start();
            }

            ResetCount++;

            await Task.Delay(-1);
        }

        private async Task ClientOnReaction(Cacheable<IUserMessage, ulong> cache, ISocketMessageChannel channel, SocketReaction reaction) {
            var message = await cache.GetOrDownloadAsync();
            var source = new WebGrab();
            //ConsoleMessage.Color($"REACTION EVENT DEBUG: {messageContent} -> {reaction.Emote}", ConsoleColor.DarkGreen);

            // Examples probably needs to be in a separate class
            // {reaction.MessageId} <-- I gets messageId containing reaction

            if (!IsNsfwDownloadDisabled) {
                ConsoleMessage.Warning($"IsNsfwDownloadDisabled cannot be false: status: {IsNsfwDownloadDisabled}");
                return;
            }
            if (reaction.Emote.Name == "😍" && (message.Content.EndsWith(".gif") || message.Content.EndsWith(".png")
                                                || message.Content.EndsWith(".gifv") || message.Content.EndsWith(".jpg")
                                                || message.Content.Contains("//imgur.com") || message.Content.Contains("gfycat.com"))) {

                using (var writer = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "imageduplication.txt"))) {
                    foreach (var urlList in UrlContainer) {
                        await writer.WriteLineAsync(urlList);
                        if (urlList.Contains(message.Content)) {
                            ConsoleMessage.Color($"Link is in duplication checklist {urlList}", ConsoleColor.Cyan);
                            return;
                        }
                    }
                }
                if (channel.Id == 582223607497883678)
                    await source.ParseImageUrls(message.Content, "nsfw");
                if (channel.Id == 566367044396646402)
                    await source.ParseImageUrls(message.Content, "nsfw_h");

                ConsoleMessage.Color($"Favorited. Image Downloaded: {message.Content}", ConsoleColor.DarkCyan);

                if (!UrlContainer.Contains(message.Content)) {
                    UrlContainer.Add(message.Content);
                } else {
                    return;
                }
                //await channel.SendMessageAsync($"Responded with a 😍 MessageContent: {message.Content}");
                //ConsoleMessage.Debug(message.Content);
            }

        }

        /// <summary>
        /// Left off: https://old.reddit.com/r/nsfw411/wiki/fulllist1 line: 1572
        /// </summary>
        private void CrawlReddit() {
            // Disabled for now. No one goes here
            //WebScraper.RedditGrabber("https://www.reddit.com/r/eyebleach+corgi+aww/new.rss", RedditRssPattern, "eyebleach", 475041086218174474);

            WebScraper.RedditGrabber("https://www.reddit.com/r/nsfw+Amateur+PetiteGoneWild+collegesluts+springbreakers+barelylegalteens+suicidegirls+skinnytail+XXX_Animated_Gifs+SocialMediaSluts+creampies+ballsdeepandcumming+creampiegifs+knockmeup+GFTJCreampie+TheBestCreampies+cumlube+porngif+BustyPetite+snapleaks+snapchatgw+GirlsFinishingTheJob+LegalTeens+LipsThatGrip+porninfifteenseconds+CumshotSelfies+OralCreampie+forcedcreampie+Just18+RealGirls+gwcumsluts+OnOff+thickloads+FacialFun+CumSwallowing+AsianHotties+DrunkGirls+nsfw2+adultgifs+Penetration_gifs+celebnsfw+FitGirlsFucking+FreshGIF+holdthemoan+WatchItForThePlot+BBCSluts+BonerMaterial+dirtysmall+girlsdoporn+tinder_sluts+SexWithStrangers+tiktoknsfw+SnapchatSingles+slutsofsnapchat+snapchat_sluts+PremiumSnapchat+HappyEmbarrassedGirls+nsfwcosplay+ginger+beachgirls+SnapchatSext+redheads+palegirls+rearpussy+quiver+adorableporn+gifsgonewild+distension+iWantToFuckHer+Exxxtras+Bottomless_Vixens+FlashingGirls+HugeDickTinyChick+throatpies+Hot_Women_Gifs+before_after_cumsluts+bodyperfection+TributeMe+Sexy_Ed+BlowjobGifs+Breeding+shewantstofuck+UnrealGirls+after_the_shot+lineups+Ifyouhadtopickone+spreading+girlswhoride+NSFW_China+IncestGifs+cfnmfetish+selfshots+emogirls+PerfectPussies+cumplay_gifs+GirlsWithiPhones+nsfwgif+TooBig+Twistys+serafuku+prematurecumshots+Squatfuck+NSFW_GFY+StreamersGoneWild+18_20+GirlsOnTop+HaveToHaveHer+smilers+JerkingHimOntoHer+TrashyPorn+FirstInsertion+NSFW_showcase+LegalCollegeGirls+movie_nudes+momson+nsfwnonporn+ImgurNSFW+doggy+nsfwxpost+gwcumselfie+ChineseHotties+Best_NSFW_Content+NoBSNSFW+dpgirls+fappitt+SnapchatXXX+JewSnap+NSFWSnapchat/new.rss", RedditRssPattern, "nsfw", 582223607497883678);
            //scraper.RedditGrabber("https://reddit.com/r/FreshGIF/new.rss", RedditRssPattern, 474337373686071306);
            WebScraper.RedditGrabber("https://www.reddit.com/r/hentai+HENTAI_GIF+rule34+DirtyGaming+PokePorn+WesternHentai+Overwatch_Porn+Tentai+Sukebei+Nekomimi+NSFWskyrim+nsfwanimegifs+HQHentai/new.rss", RedditRssPattern, "nsfw_h", 566367044396646402);
        }

        public async Task ClientConnected() {
            await Task.Delay(1000);
            IsDiscordDisconnected = false;
        }

        public async Task ClientDisconnected(Exception exception) {
            IsDiscordDisconnected = true;

            await Task.Delay(10000);

            if (InsultCooldownTask.Status != TaskStatus.Running || InsultCooldownTask.IsCanceled || InsultCooldownTask.IsFaulted) {
                ConsoleMessage.Error("InsultCooldownTask malfunctioned!");
                InsultCooldownTask.Start();
            }
            if (OsrsCooldownTask.Status != TaskStatus.Running || OsrsCooldownTask.IsCanceled || OsrsCooldownTask.IsFaulted) {
                ConsoleMessage.Error("OsrsCooldownTask malfunctioned!");
                OsrsCooldownTask.Start();
            }
            //if (RedditTask.Status != TaskStatus.Running || RedditTask.IsCanceled || RedditTask.IsFaulted) {
            //    ConsoleMessage.Error("RedditTask malfunctioned!");
            //    RedditTask.Start();
            //}
        }

        private async Task ClientReady() {
            IsNsfwDownloadDisabled = true;
            IsDiscordDisconnected = false;

            #if DEBUG
            IsRedditLoopDisabled = true;
            #endif

            await DiscordClient.SetGameAsync("Ripping PHAT clouds!", "https://twitch.tv/shroud", ActivityType.Streaming);

            var isOwnerPresent = AdminFullList.Any(owner => owner.Id == DiscordClient.GetGuild(474333807441477661).OwnerId);

            // Read through all configs I don't care for .json files just average txt.
            // Although, I may regret this later. ; _ ;
            if (!isOwnerPresent) {
                var duplicationFile = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "imageduplication.txt"));
                var censoredFile = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "censored.txt"));
                var filterFile = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "filter.txt"));
                var adminFile = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "admins.txt"));

                foreach (var line in duplicationFile) {
                    if (line == string.Empty) return;

                    UrlContainer.Add(line);
                    ConsoleMessage.Message($"Added duplication url {line}");
                }
                foreach (var line in censoredFile) {
                    if (line == string.Empty) return;

                    CensoredList.Add(new CensoredUsers(Convert.ToUInt64(line)));
                    ConsoleMessage.Message($"Added censored user {DiscordClient.GetUser(Convert.ToUInt64(line))}");
                }
                foreach (var line in filterFile) {
                    if (line == string.Empty) return;

                    FilterList.Add(new Filter(line));
                    ConsoleMessage.Message($"Added filter from file: {line}");
                }
                foreach (var line in adminFile) {
                    if (line == string.Empty) return;

                    AdminFullList.Add(new AdminUsers(Convert.ToUInt64(line)));
                    ConsoleMessage.Message($"Added Admin {DiscordClient.GetUser(Convert.ToUInt64(line))}");
                }
            }
        }
        public Task ClientLogging(LogMessage arg) {
            Console.WriteLine(arg);
            return Task.CompletedTask;
        }
        public async Task HandleCommandsAsync(SocketMessage arg) {
            if (!(arg is SocketUserMessage message))
                return;
            
            var globalContext = new SocketCommandContext(DiscordClient, message);

            if (globalContext.Message == null || globalContext.Message.Content == "") return;
            if (globalContext.User.IsBot) return;

            var disabledUser = SilencedUserList.Exists(user => user?.Id == globalContext.User.Id);

            foreach (var disabled in FilterList) {
                var censoredUser = CensoredList.Exists(user => user?.Id == globalContext.User.Id && globalContext.Message.Content.Contains(disabled.BannedWords));

                if (censoredUser || disabledUser) {
                    await globalContext.Message.DeleteAsync();
                    break;
                }

                if (censoredUser && disabledUser) {
                    await globalContext.Message.DeleteAsync();
                    break;
                }
            }

            var argPos = 0;

            // check if user is disabled if so, cannot perform command.
            if ((!disabledUser && message.HasStringPrefix("yeet ", ref argPos)) || (!disabledUser && message.HasMentionPrefix(DiscordClient.CurrentUser, ref argPos))) {
                var commandContext = new SocketCommandContext(DiscordClient, message);
                var result = await DiscordCommand.ExecuteAsync(commandContext, argPos, Providers);

                if (commandContext.Message == null || commandContext.Message.Content == string.Empty) return;
                if (commandContext.User.IsBot) return;

                if (!result.IsSuccess) {
                    #if DEBUG
                    await commandContext.Channel.SendMessageAsync("```" + "[" + DateTime.Now + "]: " +
                                                                  result.ErrorReason + "``` \n" +
                                                                  "```" + "[Message Content]: " +
                                                                  commandContext.Message.Content + "```");
                    #endif

                    #if !DEBUG
                    await commandContext.Channel.SendMessageAsync($"You've entered the wrong command. Please try again.");
                    #endif
                }
            }
        }
        public void Start() {
            RunBotAsync().GetAwaiter().GetResult();
            ConsoleMessage.Message("THIS CODE STLL WORKS FAM");
        }
    }
}