﻿using System.Diagnostics;

namespace Bot.Core.Helpers {
    public static class ShellHelper {
        /// <summary>
        ///     Function from https://loune.net/author/admin/
        ///     basically helps with running commands on bash using
        ///     dotnet core
        /// </summary>
        /// 
        /// <summary>
        ///     Not used yet but still handy to have whenever i use linux commands to this
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static string BashRun(this string cmd) {
            var escapedArgs = cmd.Replace("\"", "\\\"");
            var process = new Process {
                StartInfo = new ProcessStartInfo {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = true
                }
            };

            process.Start();

            var result = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            return result;
        }
    }
}