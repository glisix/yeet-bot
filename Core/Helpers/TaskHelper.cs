﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Bot.Core.Helpers {
    public class TaskHelper {
        public static async Task DisconnectedHangTasks(int milliseconds) {
            while (Globals.IsDiscordDisconnected) {
                ConsoleMessage.Error("Discord has disconnected. Reconnecting...");
                await Task.Delay(milliseconds);
            }
        }
    }
}
