﻿using System.Threading.Tasks;
using Discord.Commands;

namespace Bot.Core.Modules {


    // Be gentle. Don't yell at me ; _ ;



    [Group("temote")]
    
    public class CommandTemote : ModuleBase {
        [Command("help")]
        public async Task Info() {
            await ReplyAsync("yeet temote [expression] \n```diff\n" +
                             "+§ Normal Text Emotes\n" +
                             "\t lenny, lennygang, lennybignose, lennybill, secret, aye2, aye1, why, kissme, oh, tussle, orly, happydog, relaxed, blush, gottablast, bitchface, mhmm, objection, shocked, flipperson, ohno\n" +
                             "+§ Big Text Emote\n" +
                             "\t pikachu, fabulous, middlefinger, shit, ok, thumbsup" + 
                             " ```");
        }
        [Command("thumbsup")]
        public async Task Thumbsup() {
            await ReplyAsync(".\n" +
                            @"░░░░░░░░░░░░░░░░░░░░░░█████████
░░███████░░░░░░░░░░███▒▒▒▒▒▒▒▒███
░░█▒▒▒▒▒▒█░░░░░░░███▒▒▒▒▒▒▒▒▒▒▒▒▒███
░░░█▒▒▒▒▒▒█░░░░██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██
░░░░█▒▒▒▒▒█░░░██▒▒▒▒▒██▒▒▒▒▒▒██▒▒▒▒▒███
░░░░░█▒▒▒█░░░█▒▒▒▒▒▒████▒▒▒▒████▒▒▒▒▒▒██
░░░█████████████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██
░░░█▒▒▒▒▒▒▒▒▒▒▒▒█▒▒▒▒▒▒▒▒▒█▒▒▒▒▒▒▒▒▒▒▒██
░██▒▒▒▒▒▒▒▒▒▒▒▒▒█▒▒▒██▒▒▒▒▒▒▒▒▒▒██▒▒▒▒██
██▒▒▒███████████▒▒▒▒▒██▒▒▒▒▒▒▒▒██▒▒▒▒▒██
█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▒▒▒▒▒▒████████▒▒▒▒▒▒▒██
██▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██
░█▒▒▒███████████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██
░██▒▒▒▒▒▒▒▒▒▒████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
░░████████████░░░█████████████████");
        }
        [Command("ok")]
        public async Task ok() {
            await ReplyAsync(".\n" +
                            @"────────────────────────────────────────
────────────────────────────────────────
───────────████──███────────────────────
──────────█████─████────────────────────
────────███───███───████──███───────────
────────███───███───██████████──────────
────────███─────███───████──██──────────
─────────████───████───███──██──────────
──────────███─────██────██──██──────────
──────██████████────██──██──██──────────
─────████████████───██──██──██──────────
────███────────███──██──██──██──────────
────███─████───███──██──██──██──────────
───████─█████───██──██──██──██──────────
───██████───██──────██──────██──────────
─████████───██──────██─────███──────────
─██────██───██─────────────███──────────
─██─────███─██─────────────███──────────
─████───██████─────────────███──────────
───██───█████──────────────███──────────
────███──███───────────────███──────────
────███────────────────────███──────────
────███───────────────────███───────────
─────████────────────────███────────────
──────███────────────────███────────────
────────███─────────────███─────────────
────────████────────────██──────────────
──────────███───────────██──────────────
──────────████████████████──────────────
──────────████████████████─────────────");
        }
        [Command("shit")]
        public async Task Shit() {
            await ReplyAsync(".\n" +
                            @"░░░░░░░░░░░█▀▀░░█░░░░░░
░░░░░░▄▀▀▀▀░░░░░█▄▄░░░░
░░░░░░█░█░░░░░░░░░░▐░░░
░░░░░░▐▐░░░░░░░░░▄░▐░░░
░░░░░░█░░░░░░░░▄▀▀░▐░░░
░░░░▄▀░░░░░░░░▐░▄▄▀░░░░
░░▄▀░░░▐░░░░░█▄▀░▐░░░░░
░░█░░░▐░░░░░░░░▄░█░░░░░
░░░█▄░░▀▄░░░░▄▀▐░█░░░░░
░░░█▐▀▀▀░▀▀▀▀░░▐░█░░░░░
░░▐█▐▄░░▀░░░░░░▐░█▄▄░░░
░░░▀▀▄░░░░░░░░▄▐▄▄▄▀░░░
░░░░░░░░░░░░░░░░░░░░░░░");
        }

        [Command("middlefinger")]
        public async Task MiddleFinger() {
            await ReplyAsync(".\n" +
                            @"░░░░░░░░░░░░░░░▄▄░░░░░░░░░░░
░░░░░░░░░░░░░░█░░█░░░░░░░░░░
░░░░░░░░░░░░░░█░░█░░░░░░░░░░
░░░░░░░░░░░░░░█░░█░░░░░░░░░░
░░░░░░░░░░░░░░█░░█░░░░░░░░░░
██████▄███▄████░░███▄░░░░░░░
▓▓▓▓▓▓█░░░█░░░█░░█░░░███░░░░
▓▓▓▓▓▓█░░░█░░░█░░█░░░█░░█░░░
▓▓▓▓▓▓█░░░░░░░░░░░░░░█░░█░░░
▓▓▓▓▓▓█░░░░░░░░░░░░░░░░█░░░░
▓▓▓▓▓▓█░░░░░░░░░░░░░░██░░░░░
▓▓▓▓▓▓█████░░░░░░░░░██░░░░░░");
        }

        [Command("fabulous")]
        public async Task Fabulous() {
            await ReplyAsync(@".

⊂_ヽ
　 ＼＼ 
　　 ＼( ͡° ͜ʖ ͡°)
　　　 >　⌒ヽ
　　　/ 　 へ＼
　　 /　　/　＼＼
　　 ﾚ　ノ　　 ヽ_つ
　　/　/
　 /　/|
　(　(ヽ
　|　|、＼
　| 丿 ＼ ⌒)
　| |　　) /
ノ )　　Lﾉ
(_／         ");
        }

        [Command("pikachu")]
        public async Task Pikachu() {
            await ReplyAsync(
                ".\r\n█▀▀▄░░░░░░░░░░░▄▀▀█\r\n░█░░░▀▄░▄▄▄▄▄░▄▀░░░█\r\n░░▀▄░░░▀░░░░░▀░░░▄▀\r\n░░░░▌░▄▄░░░▄▄░▐▀▀\r\n░░░▐░░█▄░░░▄█░░▌▄▄▀▀▀▀█ #Pikachu\r\n░░░▌▄▄▀▀░▄░▀▀▄▄▐░░░░░░█\r\n▄▀▀▐▀▀░▄▄▄▄▄░▀▀▌▄▄▄░░░█\r\n█░░░▀▄░█░░░█░▄▀░░░░█▀▀▀\r\n░▀▄░░▀░░▀▀▀░░▀░░░▄█▀\r\n░░░█░░░░░░░░░░░▄▀▄░▀▄\r\n░░░█░░░░░░░░░▄▀█░░█░░█\r\n░░░█░░░░░░░░░░░█▄█░░▄▀\r\n░░░█░░░░░░░░░░░████▀\r\n░░░▀▄▄▀▀▄▄▀▀▄▄▄█▀");
        }

        [Command("lenny")]
        public async Task Lenny() {
            await ReplyAsync("( ͡° ͜ʖ ͡°)");
        }

        [Command("lennygang")]
        public async Task LennyGang() {
            await ReplyAsync("( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)");
        }

        [Command("lennybignose")]
        public async Task LennyBigNose() {
            await ReplyAsync("(͡ ͡° ͜ つ ͡͡°)");
        }

        [Command("lennybill")]
        public async Task LennyBill() {
            await ReplyAsync("[̲̅$̲̅(̲̅ ͡° ͜ʖ ͡°̲̅)̲̅$̲̅]");
        }

        [Command("secret")]
        public async Task Secret() {
            await ReplyAsync("(¬‿¬)");
        }

        [Command("aye2")]
        public async Task Aye() {
            await ReplyAsync("(☞ﾟヮﾟ)☞ ☜(ﾟヮﾟ☜)");
        }

        [Command("aye1")]
        public async Task Aye2() {
            await ReplyAsync("(☞ﾟ∀ﾟ)☞");
        }

        [Command("why")]
        public async Task Why() {
            await ReplyAsync("ಠ_ಠ");
        }

        [Command("kissme")]
        public async Task KissMe() {
            await ReplyAsync("(づ￣ ³￣)づ");
        }

        [Command("oh")]
        public async Task Oh() {
            await ReplyAsync("(• ε •)");
        }

        [Command("tussle")]
        public async Task Tussle() {
            await ReplyAsync("(ง'̀-'́)ง");
        }

        [Command("orly")]
        public async Task Orly() {
            await ReplyAsync("﴾͡๏̯͡๏﴿ O'RLY?");
        }

        [Command("happydog")]
        public async Task HappyDog() {
            await ReplyAsync("(ᵔᴥᵔ)");
        }

        [Command("relaxed")]
        public async Task Relaxed() {
            await ReplyAsync("(￣▽￣)﻿");
        }

        [Command("blush")]
        public async Task Blush() {
            await ReplyAsync("(▰˘◡˘▰)");
        }

        [Command("gottablast")]
        public async Task GottaBlast() {
            await ReplyAsync("♪~ ᕕ(ᐛ)ᕗ");
        }

        [Command("bitchface")]
        public async Task BitchFace() {
            await ReplyAsync("¬_¬");
        }

        [Command("mhmm")]
        public async Task Mhmm() {
            await ReplyAsync("ಠ⌣ಠ");
        }

        [Command("objection")]
        public async Task Objection() {
            await ReplyAsync("(°ロ°)☝");
        }

        [Command("shocked")]
        public async Task Shocked() {
            await ReplyAsync("◉_◉");
        }

        [Command("flipperson")]
        public async Task FlipPerson() {
            await ReplyAsync("（╯°□°）╯︵( .o.)");
        }

        [Command("ohno")]
        public async Task Ohno() {
            await ReplyAsync("(ʘᗩʘ')");
        }
    }
}