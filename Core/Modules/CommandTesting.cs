﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

#if DEBUG
namespace Bot.Core.Modules {

    [Group("test")]
    public class CommandTesting : ModuleBase {

        [Command]
        public async Task Testing() {
            var letesting = "<test>";
            var newtest = letesting.Trim('<','>');
            

            await ReplyAsync(newtest);
        }
    }
}
#endif