﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Bot.Core.Services;

namespace Bot.Core.Modules {

    [Group("music")]
    public class CommandMusic : ModuleBase {

        private readonly AudioServices _services;
        private readonly Random _random;

        public bool IsAdminExist => Globals.AdminFullList.Exists(v => v.Id == Context.User.Id);

        public CommandMusic(AudioServices services) {
            _services = services;
            _random = new Random();
        }

        [Command("play", RunMode = RunMode.Async)]
        public async Task PlayCmd([Remainder] string song) {
            var directory = string.Empty;
            if (IsAdminExist) {
                ConsoleMessage.Warning($"Musicbot is active? {AudioServices.IsMusicActive}");
                await _services.JoinAudio(Context.Guild, (Context.User as IVoiceState)?.VoiceChannel);

                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
                    directory = Path.Combine($@"{AppDomain.CurrentDomain.BaseDirectory}music/test/{song}");
                }
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
                    directory = Path.Combine($@"{AppDomain.CurrentDomain.BaseDirectory}music\{song}");
                }

                if (!AudioServices.IsMusicActive) {
                    AudioServices.IsMusicActive = true;

                    await _services.SendAudioAsync(Context.Guild, Context.Channel, directory);
                    await _services.LeaveAudio(Context.Guild);
                } else {
                    await ReplyAsync("The bot is still playing music. Stop the bot and then run the command again");
                }
            }
            else {
                await ReplyAsync("Not an admin.");
            }
        }
        [Command("shuffle", RunMode = RunMode.Async)]
        public async Task ShuffleRepeat() {
            if (!IsAdminExist) {
                await ReplyAsync("Not an admin.");
                return;
            }
            var files = _services.AudioFolder();
            if (files == null) {
                await ReplyAsync("File Not Available");
            }
            await _services.JoinAudio(Context.Guild, (Context.User as IVoiceState)?.VoiceChannel);
            if (!AudioServices.IsMusicActive) {
                AudioServices.IsMusicActive = true;
                var music = files?[_random.Next(0, files.Length)];
                while (true) {
                    //await Service.AudioClient.SetSpeakingAsync(true);
                    await _services.SendAudioAsync(Context.Guild, Context.Channel, music?.FullName);
                    await _services.ClearAudio();
                    music = files?[_random.Next(0, files.Length)];
                }
            }
            else {
                await ReplyAsync("Bot is still playing music.");
            }
        }
        [Command("list", RunMode = RunMode.Async)]
        public async Task SongList() {
            var files = _services.AudioFolder();
            var builder = new StringBuilder();

            builder.Append("```");
            foreach (var file in files) {
                builder.Append($"{file.Name}\n");
            }
            builder.Append("```");
            await ReplyAsync(builder.ToString());
        }

        [Command("stop")]
        public async Task Leave() {
            await _services.LeaveAudio(Context.Guild);
        }

    }
}
