﻿using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

using Discord.Commands;

namespace Bot.Core.Modules {
    [Group("osrs")]
    public class CommandOsrs : ModuleBase {
        /// <summary>
        /// I now have HTMLAgility Pack and am too lazy to redo this.
        /// </summary>

        private string _osrsPattern => "<td align=\"right\"><img\\sclass=\"miniimg\".*\n.*\n(.*)\n.*\n<td\\salign=\"right\">(.*)<\\/td>\n<td\\salign=\"right\">(.*)<\\/td>\n<td\\salign=\"right\">(.*)<\\/td>";

        /// <summary>
        ///  A little help from https://docs.microsoft.com/en-us/dotnet/api/system.string.format?view=netframework-4.7.2
        /// </summary>
        /// <param name="osrsUser"></param>
        /// <returns></returns>
        [Command("stats")]
        public async Task OsrsStatsAll([Remainder]string osrsUser) {
            var osrsWeb = new WebGrab($"https://secure.runescape.com/m=hiscore_oldschool/hiscorepersonal.ws?user1={osrsUser}");
            var osrsRegex = Regex.Matches(osrsWeb.WebResponse, _osrsPattern);

            if (Globals.OsrsCooldownTimer < 3) {
                var builder = new StringBuilder();
                var header = $"{"Skill",-20}{"Rank":12}{"Level",7}{"XP",13}\n";

                builder.Append($"osrs stats for: [{osrsUser}] \n```js\n{header} \n");

                foreach (Match match in osrsRegex) {
                    var ability = match.Groups[1].Value;
                    var rank = match.Groups[2].Value;
                    var level = match.Groups[3].Value;
                    var xp = match.Groups[4].Value;

                    var stats = new[] {
                        Tuple.Create(ability, rank, level, xp)
                    };
                    foreach (var colStats in stats) {
                        var output = $"{colStats.Item1,-12}{colStats.Item2,12}{colStats.Item3,7}{colStats.Item4,13}";
                        ConsoleMessage.Debug(output);
                        builder.Append($"{output} \n");
                    }
                }
                builder.Append("```");
                await ReplyAsync(builder.ToString());
            }
            else {
                await ReplyAsync($"Exceeded command limit. Please wait {Globals.OsrsCooldownTimer}");
            }
            Globals.OsrsCooldownTimer++;
        }
    }
}