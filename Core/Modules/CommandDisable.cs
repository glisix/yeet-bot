﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Bot.Core.Models;

namespace Bot.Core.Modules {

    [Group("disable")]
    public class CommandDisable : ModuleBase {
        public bool IsAdminExist => Globals.AdminFullList.Exists(a => a.Id == Context.User.Id);

        [Command]
        public async Task Disable() {
            if (IsAdminExist)
                await Context.Channel.SendMessageAsync("Disable who? Use the @ to disable a users tenor gif" +
                                                       "```" +
                                                       @"Example: yeet disable gif @user" +
                                                       "```");
            else
                await Context.Channel.SendMessageAsync("```Your ID isn\'t whitelisted to use that command```");
        }

        [Command]
        public async Task Disable(IGuildUser user) {
            if (IsAdminExist) {
                var containsItem = Globals.CensoredList.Any(item => item.Id == user.Id);
                if (!containsItem) {
                    Globals.SilencedUserList.Add(new Disabled(user.Username, user.Id));
                    ConsoleMessage.Message($"{user.Mention} has been silenced!");
                    await ReplyAsync($"{user.Mention} has been silenced!");
                }
            }
            else {
                await Context.Channel.SendMessageAsync("```Your ID isn\'t whitelisted to use that command```");
            }
        }

        [Command("download", RunMode = RunMode.Async)]
        public async Task RedditUrlDownload(string value) {
            if (IsAdminExist) {
                if (value.Equals("true")) {
                    Globals.IsNsfwDownloadDisabled = true;
                    await ReplyAsync("Nsfw downloads stopped!");
                }

                if (value.Equals("false")) {
                    Globals.IsNsfwDownloadDisabled = false;
                    await ReplyAsync("Nsfw downloads started!");
                }
            }
        }
        [Command("reddit", RunMode = RunMode.Async)]
        public async Task RedditLoop(string value) {
            if (IsAdminExist) {
                if (value.Equals("true")) {
                    Globals.IsRedditLoopDisabled = true;
                    await ReplyAsync("Reddit Loop has stopped");
                }

                if (value.Equals("false")) {
                    Globals.IsRedditLoopDisabled = false;
                    await ReplyAsync("Reddit Loop has continued");
                }
            }
        }
    }
}