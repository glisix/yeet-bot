﻿using System.Threading.Tasks;

using Discord;
using Discord.Commands;

namespace Bot.Core.Modules {
    [Group("enable")]
    public class CommandEnable : ModuleBase {
        public bool IsAdminExist => Globals.AdminFullList.Exists(a => a.Id == Context.User.Id);

        [Command]
        public async Task Enable() {
            if (IsAdminExist)
                await Context.Channel.SendMessageAsync("Enable who? Use the @ to enable a users tenor gif" +
                                                       "```" +
                                                       @"Example: yeet disable gif @nick" +
                                                       "```");
            else
                await Context.Channel.SendMessageAsync("```Your ID isn\'t whitelisted to use that command```");
        }

        [Command]
        public async Task Enable(IGuildUser user) {
            if (IsAdminExist) {
                Globals.SilencedUserList.RemoveAll(x => x.Id == user.Id);
                ConsoleMessage.Message("Removed username: " + user.Username + " | ID: " + user.Id);
                await ReplyAsync($"{user.Username} Can now talk again");
            }
            else {
                await Context.Channel.SendMessageAsync("```Your ID isn\'t whitelisted to use that command```");
            }
        }
    }
}