﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Discord.Commands;

using Bot.Core.Models;

namespace Bot.Core.Modules {

    [Group("filter")]
    public class CommandFilter : ModuleBase {

        public bool IsAdminExist => Globals.AdminFullList.Exists(a => a.Id == Context.User.Id);

        //TODO: Remove the thumbnail for each post

        [Command("add", RunMode = RunMode.Async)]
        public async Task AddFilterBan([Remainder]string bannedWord) {
            var newBannedWord = bannedWord.Trim('<', '>');
            var isBannedWordExist = Globals.FilterList.Exists(exist => exist.BannedWords == newBannedWord);

            if (IsAdminExist) {
                if (!isBannedWordExist) {
                    if (newBannedWord.Contains("http://") || newBannedWord.Contains("https://")) {
                        Globals.FilterList.Add(new Filter(newBannedWord));
                        await ReplyAsync($"\"<{newBannedWord}>\" has been added to filter list");
                    } else {
                        Globals.FilterList.Add(new Filter(newBannedWord));
                        await ReplyAsync($"\"{newBannedWord}\" has been added to filter list");
                    }

                    using (var writer = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "filter.txt"))) {
                        foreach (var filter in Globals.FilterList) {
                            writer.WriteLine($"{filter.BannedWords}");
                        }
                    }

                } else {
                    await ReplyAsync("filter already exists");
                }
            } else {
                await ReplyAsync("You are not an admin");
            }
        }

        [Command("list")]
        public async Task FilterHelp() {
            var builder = new StringBuilder();
            if (Globals.FilterList.Count != 0) {
                builder.Append("```");
                foreach (var filter in Globals.FilterList) {
                    builder.Append($"{filter.BannedWords}\n");
                }
                builder.Append("```");
                await ReplyAsync($"{builder.ToString()}");
            } else {
                await ReplyAsync("Filter list is empty");
            }
        }

        [Command("remove")]
        public async Task RemFilterBan([Remainder]string bannedWord) {
            var newBannedWord = bannedWord.Trim('<', '>');
            var isBannedWordExist = Globals.FilterList.Exists(exist => exist.BannedWords == newBannedWord);

            if (IsAdminExist) {
                if (isBannedWordExist) {
                    if (newBannedWord.Contains("http://") || newBannedWord.Contains("https://")) {
                        ConsoleMessage.Debug(newBannedWord);
                        Globals.FilterList.RemoveAll(v => v.BannedWords == newBannedWord);
                        await ReplyAsync($"Removed from filter: <{newBannedWord}>");
                    } else {
                        Globals.FilterList.RemoveAll(v => v.BannedWords == newBannedWord);
                        await ReplyAsync($"Removed from filter: {newBannedWord}");
                    }
                    using (var writer = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "filter.txt"))) {
                        foreach (var filter in Globals.FilterList) {
                            writer.WriteLine($"{filter.BannedWords}");
                        }
                    }
                } else {
                    await ReplyAsync("Not on filter");
                }
            } else {
                await ReplyAsync("Not an admin");
            }
        }
    }
}
