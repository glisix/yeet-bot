﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Bot.Core.Models;


namespace Bot.Core.Modules {

    [Group("admin")]
    public class CommandAdmin : ModuleBase {

        [Command("add")]
        public async Task AdminAdd(IGuildUser user) {
            var isAdminPresent = Globals.AdminFullList.Exists(v => v.Id == Context.User.Id);
            var isAdminExist = Globals.AdminFullList.Exists(v => v.Id == user.Id);

            if (isAdminPresent) {
                if (!isAdminExist) {
                    Globals.AdminFullList.Add(new AdminUsers(user.Username, user.Id));
                    using (var writer = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "admins.txt"))) {
                        foreach (var adminList in Globals.AdminFullList) {
                            writer.WriteLine($"{adminList.Id}");
                        }
                    }
                    await ReplyAsync($"Admin added: {Globals.DiscordClient.GetUser(user.Id)}");
                    ConsoleMessage.Message($"Admin added: {Globals.DiscordClient.GetUser(user.Id)}");
                }
                else {
                    await ReplyAsync("Admin already exists");
                }
            }
            else {
                ConsoleMessage.Warning("Non admin attempted command");
                await ReplyAsync("You're not an admin");
            }
        }

        [Command("remove")]
        public async Task AdminRemove(IGuildUser user) {
            var isAdminPresent = Globals.AdminFullList.Exists(v => v.Id == Context.User.Id);
            var isAdminExist = Globals.AdminFullList.Exists(v => v.Id == user.Id);

            if (isAdminPresent) {
                if (isAdminExist) {
                    Globals.AdminFullList.RemoveAll(v => v.Id == user.Id);

                    using (var writer = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "admins.txt"))) {
                        foreach (var adminList in Globals.AdminFullList) {
                            writer.WriteLine($"{adminList.Id}");
                        }
                    }
                    ConsoleMessage.Message($"Admin removed: {Globals.DiscordClient.GetUser(user.Id)}");
                    await ReplyAsync($"Admin removed: {Globals.DiscordClient.GetUser(user.Id)}");
                } else {
                    await ReplyAsync("Admin does not exist in the list");
                }
            } else {
                await ReplyAsync("You are not an admin");
            }
        }
        [Command("list")]
        public async Task AdminList() {
            if (Globals.AdminFullList.Count != 0) {
                var builder = new StringBuilder();
                builder.Append("```");
                foreach (var adminList in Globals.AdminFullList) {
                    builder.Append($"{Globals.DiscordClient.GetUser(adminList.Id)} \n");
                    ConsoleMessage.Message($"{Globals.DiscordClient.GetUser(adminList.Id)}");
                }
                builder.Append("```");
                await ReplyAsync(builder.ToString());
            } else {
                ConsoleMessage.Warning("Admin list is empty");
                await ReplyAsync("Admin list is empty");
            }
        }
    }
}
