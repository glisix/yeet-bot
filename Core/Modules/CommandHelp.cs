﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

namespace Bot.Core.Modules {

    [Group("help")]
    public class CommandHelp : ModuleBase {

        [Command]
        public async Task Help() {
            await ReplyAsync("```diff\n"+
                "+§ Old School Runescape stats viewer\n" +
                "\t- yeet osrs stats (osrs username)\n\n"+
                "+§ Insult Generator - Insult someone you don't like\n" +
                "\t- yeet insult @user\n\n"+
                "+§ Text Emotes - Express your emotion through text\n" +
                "\t- yeet temote (expression)\n"+
                "\t- yeet temote help - views the list of expressions\n\n"+
                "+§ Music Bot - Plays music from my music folder [MUST BE IN VOICE CHANNEL]\n" +
                "\t- yeet music play (music file) [ADMIN ONLY] - until discord.net fixes bug\n"+
                "\t- yeet music shuffle - [BUG] - Flickering speakerlight fix till next discord.net update\n"+
                "\t- yeet music stop - stops the music and disconnect from voice channel\n"+
                "\t- yeet music list - views the list of music files\n\n"+
                "+§ Filter - Adds or removes banned words including links excluding attachments\n" +
                "\t- yeet filter add (bannedwords) - [ADMIN ONLY] - Adds a banned word\n"+
                "\t- yeet filter remove (bannedwords) - [ADMIN ONLY] - Removes a banned word\n" +
                "\t- yeet filter list - views the list of banned words\n\n"+
                "+§ Censor - Censors an individual user. The banned words filter applies to the user\n" +
                "\t- yeet censor @user - [ADMIN ONLY] - censors an individual user\n"+
                "\t- yeet decensor @user - [ADMIN ONLY] - removes a user from being censored\n" +
                "\t- yeet censor list - views the list of people who are censored\n\n"+
                "+§ Disable and Enable - Adds or Removes a/an @user to Silence\n" +
                "\t- yeet disable @user - [ADMIN ONLY] - completely silences a user from server\n"+
                "\t- yeet disable redditloop (true/false) - [ADMIN ONLY] - stops the flow of reddit images\n"+
                "\t- yeet enable @user - [ADMIN ONLY] - unsilences user\n\n"+
                "+§ Admins - Adds or Removes an admin for the bot\n" +
                "\t- yeet admin add @user - [ADMIN ONLY/OWNER] - adds a user for admin only commands\n"+
                "\t- yeet admin remove @user - [ADMIN ONLY/OWNER] - removes user from admin only commands\n"+
                "\t- yeet admin list - views the admins for the bot"+
                "```");
        }

    }
}
