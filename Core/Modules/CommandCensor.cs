﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Bot.Core.Models;

//using Models;
//using Core;

namespace Bot.Core.Modules {

    [Group("censor")]
    public class CommandCensor : ModuleBase {
        public bool IsAdminExist => Globals.AdminFullList.Exists(a => a.Id == Context.User.Id);

        [Command]
        public async Task Censor(IGuildUser user) {
            if (IsAdminExist) {
                var containsItem = Globals.CensoredList.Any(item => item.Id == user.Id);
                if (!containsItem) {
                    Globals.CensoredList.Add(new CensoredUsers(user.Username, user.Id));

                    using (var writer = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "censored.txt"))) {
                        foreach (var filter in Globals.CensoredList) {
                            writer.WriteLine($"{filter.Id}");
                        }
                    }
                    ConsoleMessage.Message($"Added username: {user.Username} | ID: {user.Id}");
                    await ReplyAsync($"{user.Mention} has been censored");
                } else {
                    await ReplyAsync("This user is already censored!");
                }
            } else {
                await ReplyAsync("```Your ID isn\'t whitelisted to use that command```");
            }
        }
        [Command("list")]
        public async Task CensorList() {
            var builder = new StringBuilder();
            if (Globals.CensoredList.Count != 0) {
                builder.Append("```");
                foreach (var censored in Globals.CensoredList) {
                    builder.Append($"{Globals.DiscordClient.GetUser(censored.Id)}\n");
                }
                builder.Append("```");
                await ReplyAsync($"{builder.ToString()}");
            } else {
                await ReplyAsync("Censor list is Empty");
            }
        }
    }

    [Group("decensor")]
    public class CommandDecensor : ModuleBase {
        public bool IsAdminExist => Globals.AdminFullList.Exists(admin => admin.Id == Context.User.Id);

        [Command]
        public async Task Decensor(IGuildUser user) {
            if (IsAdminExist) {
                Globals.CensoredList.RemoveAll(x => x.Id == user.Id);

                using (var writer = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "censored.txt"))) {
                    foreach (var filter in Globals.CensoredList) {
                        writer.WriteLine($"{filter.Id}");
                    }
                }

                ConsoleMessage.Message("Removed username: " + user.Username + " | ID: " + user.Id);
                await ReplyAsync($"{user.Username} is decensored");
            } else {
                await Context.Channel.SendMessageAsync("```Your ID isn\'t whitelisted to use that command```");
            }
        }
    }
}
