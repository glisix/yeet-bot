﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

using Discord;
using Discord.Commands;

namespace Bot.Core.Modules {

    [Group("insult")]
    public class CommandInsultGenerator : ModuleBase {

        [Command]
        public async Task Insult() {
            await ReplyAsync($"```type yeet insult @user to insult someone you don't like```");
        }

        [Command]
        public async Task Insult(IGuildUser user) {
            var random = new Random();
            var randomWeb = random.Next(0, 2);
            //ConsoleMessage.Message($"Random number is: {randomWeb}");

            if (Globals.InsultCooldownTimer <= 2) {
                switch (randomWeb) {
                    case 0:
                        await Globals.Insulter.InsultUser(Context.Channel, user, new WebGrab("http://www.pangloss.com/seidel/Shaker/"), "<p><font\\ssize=\"\\+2\">\\n(.*?)<");
                        break;
                    case 1:
                        await Globals.Insulter.InsultUser(Context.Channel, user, new WebGrab("https://evilinsult.com/generate_insult.php?lang=en&type=json"), "\"insult\":\"(.*?)\"");
                        break;
                    case 2:
                        await Globals.Insulter.InsultUser(Context.Channel, user, new WebGrab("http://autoinsult.com/index.php?style=2"), "class=\"insult\"\\sid=\"insult\">(.*?)<");
                        break;
                }
            }
            else {
                await ReplyAsync($"Exceeded command limit. Please wait {Globals.InsultCooldownTimer}");
            }
            Globals.InsultCooldownTimer++;
        }
    }
}
