﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Threading;

namespace Bot.Core {
    internal static class WebHelp {
        public static string GetFullResponse(this WebRequest webRequest) {
            var response = string.Empty;
            var random = new Random();

            Thread.Sleep(random.Next(500, 2000));

            try {
                using (var timeRes = webRequest?.GetResponse()) {
                    using (var timeResReader = new StreamReader(timeRes.GetResponseStream() ?? throw new InvalidOperationException())) {
                        response = timeResReader.ReadToEnd();
                    }
                }
            } catch(Exception ex) {
                ConsoleMessage.Error(ex.StackTrace);
                ConsoleMessage.Error("Aborting current webRequest");
                webRequest.Abort();
            }
            return response;
        }
    }

    public class WebGrab {

        public string WebResponse { get; set; }
        public string Link { get; set; }
        public WebGrab() { }
        public WebGrab(string link) {
            Link = link;
            WebRequest.DefaultWebProxy = null;
            WebResponse = HttpUtility.HtmlDecode(WebRequest.CreateHttp(link)?.GetFullResponse());
        }

        private async Task WriteToFile(string fileName) {
            using (var writer = new StreamWriter(fileName))
                foreach (var urlList in Globals.UrlContainer)
                    await writer.WriteLineAsync(urlList);
        }

        public async Task ParseImageUrls(string url, string category) {
            if (Globals.IsDiscordDisconnected)
                return;

            if (!Globals.UrlContainer.Contains(url))
                Globals.UrlContainer.Add(url);
            else
                return;

            await WriteToFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "imageduplication.txt"));

            var random = new Random();
            var newUrl = url;

            try {
                string extension;
                if (url.EndsWith(".gifv")) {
                    extension = ".mp4";
                    var gifvToMp4 = $"{newUrl.Remove(newUrl.Length - 5)}{extension}";
                    
                    ConsoleMessage.Color($"Downloading and converting Imgur \".gifv\" to \".mp4\": {gifvToMp4}", ConsoleColor.Cyan);
                    if (category == "nsfw") {
                        SaveImageToPath(gifvToMp4, 
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"reddit/videos/nsfw/{Globals.RandomString(20)}{extension}"),
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"reddit/videos/nsfw/"));
                    } else {
                        SaveImageToPath(gifvToMp4, 
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"reddit/videos/nsfw_h/{Globals.RandomString(20)}{extension}"), 
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "reddit/videos/nsfw_h/"));
                    }
                    ConsoleMessage.Message($"Done.. File name: {Globals.GetCurrentRandomString}{extension}");
                } else if (url.EndsWith(".gif") || url.EndsWith(".png") || url.EndsWith(".jpg")) {
                    extension = url.Substring(url.Length - 4);
                    ConsoleMessage.Message($"Downloading Image: {url}");
                    if (category == "nsfw") {
                        SaveImageToPath(newUrl, 
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"reddit/images/nsfw/{Globals.RandomString(20)}{extension}"),
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"reddit/images/nsfw/"));
                    } else {
                        SaveImageToPath(newUrl, 
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"reddit/images/nsfw_h/{Globals.RandomString(20)}{extension}"),
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "reddit/images/nsfw_h/"));
                    }
                    ConsoleMessage.Message($"Done.. File name: {Globals.GetCurrentRandomString}{extension}");
                }
                if (url.Contains("gfycat.com")) {
                    if (category == "nsfw") {
                        SaveImageToPath(url, 
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"reddit/videos/nsfw/{Globals.RandomString(20)}.mp4"),
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"reddit/videos/nsfw/"));
                    } else {
                        SaveImageToPath(url, 
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"reddit/videos/nsfw_h/{Globals.RandomString(20)}.mp4"), 
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "reddit/videos/nsfw_h/"));
                    }
                    ConsoleMessage.Message($"Done.. File name: {Globals.GetCurrentRandomString}.mp4");
                }
                if (url.Contains("//imgur.com")) {
                    //TODO: Later
                }
            } catch(Exception ex) {
                ConsoleMessage.Error(ex.StackTrace);
            }
            await Task.Delay(random.Next(2000, 4000));
        }
        public void SaveImageToPath(string url, string savePath, string basedirectory) {
            var organize = new OrganizeDirectory(savePath, basedirectory);
            using (WebClient client = new WebClient()) {
                if (url.Contains("gfycat.com")) {
                    var gfycatRequest = new WebGrab(url);
                    var gfycatPattern = Regex.Match(gfycatRequest.WebResponse, "<source\\ssrc=\"(.*?)\\.webm\"\\stype=\"video\\/webm\"");
                    var gfycatToMp4 = $"{gfycatPattern.Groups[1].Value}.mp4";

                    ConsoleMessage.Color($"Downloading and converting gfycat video to \".mp4\": {gfycatToMp4}", ConsoleColor.Magenta);
                    try {
                        client.DownloadFile(new Uri(gfycatToMp4), savePath);
                    } catch (Exception ex) { ConsoleMessage.Error(ex.StackTrace); }
                } else {
                    try {
                        client.DownloadFile(new Uri(url), savePath);
                    } catch (Exception ex) { ConsoleMessage.Error(ex.StackTrace); }
                }
            }
            organize.DeleteDuplicateFiles();
        }
    }
}