﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Threading;

namespace Bot.Core {
    public class OrganizeDirectory {
        private byte[] _fileHash1;
        private byte[] _fileHash2;
        public string FileLocation { get; private set; } // Gets the full name of the file location
        public string BaseDirectory { get; private set; } // Gets the full directory name(Not including the file)

        public OrganizeDirectory() { }

        public OrganizeDirectory(string filePath) {
            FileLocation = filePath;
        }
        public OrganizeDirectory(string filePath, string directory) {
            FileLocation = filePath; // File Location full directory including the filename
            BaseDirectory = directory; // Just the Directory no file included
        }

        private FileInfo[] DirectoryFolder() {
            var info = new DirectoryInfo(BaseDirectory);
            return info.Exists ? info.GetFiles("*.*") : null;
        }

        public bool IsFileEqual(string firstFile, string secondFile) {
            var hash = (HashAlgorithm)CryptoConfig.CreateFromName("SHA1");
            //var hash = HashAlgorithm.Create();

            //NOTE: Make sure that other functions aren't using the file, or FileStream will complain
            //NOTE: Make sure you don't Download async on WebClient if you're using this method
            using (FileStream fileStream1 = File.OpenRead(firstFile),
                    fileStream2 = File.OpenRead(secondFile)) {

                _fileHash1 = hash.ComputeHash(fileStream1);
                _fileHash2 = hash.ComputeHash(fileStream2);
                //ConsoleMessage.Warning($"Collecting Hash in Directory: {BitConverter.ToString(_fileHash2)}");
            }
            // TODO: Need to make a hash list now.
            //       Looping and opening all files in a directory won't be effecient.
            //       Save the hashlist in txt file I guess. sha1files.txt
            return BitConverter.ToString(_fileHash1) == BitConverter.ToString(_fileHash2);
        }

        public void DeleteDuplicateFiles() {
            var files = DirectoryFolder();
            if (files == null)
                return;
            ConsoleMessage.Debug("Scanning and deleting duplicate files");
            foreach (var file in files) {
                if (IsFileEqual(FileLocation, $"{file.FullName}")) {
                    if (file.FullName == FileLocation) {
                        ConsoleMessage.Debug($"IGNORNING ORIGINAL FILE: {file.Name}");
                    } else {
                        ConsoleMessage.Error($"DELETING: {file.Name}");
                        File.Delete(file.FullName);
                    }
                }
            }
           // TODO: Need to make a hash list now.
           //       Looping and opening all files in a directory won't be effecient.
           //       Save the hashlist in txt file I guess. sha1files.txt
        }
    }
}
