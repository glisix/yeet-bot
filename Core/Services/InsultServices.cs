﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

using Discord;


namespace Bot.Core.Services {
    public class InsultServices {

        public async Task InsultUser(IMessageChannel channel,IGuildUser user, WebGrab webGrab, string pattern) {
            var match = Regex.Match(webGrab.WebResponse, pattern);
            var result = HttpUtility.HtmlDecode(match.Groups[1].Value);

            await channel.SendMessageAsync($"{user.Mention} {result}");
        }
    }
}
