﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Bot.Core.Helpers;

namespace Bot.Core.Services {

    public class WebScraperServices {
        public async Task RedditGrabber(string urlPage, string pattern, string category, ulong channelId) {
            // Pass the groupImage to the firstStringUrl to check if the same url as first iteration
            var verifiedImageUrl = string.Empty;
            // Hold a list of URL's to check for duplications. Limit URL

            var urlContainer = new List<string> {
                Capacity = 500
            };

            while (true) {
                // if discord disconnects halt the task here till discord reconnects. 
                // We can't post a message while discord isn't connected
                await TaskHelper.DisconnectedHangTasks(10000);

                var random = new Random();

                #if DEBUG

                Globals.IsNsfwDownloadDisabled = true;
                if (Globals.IsNsfwDownloadDisabled)
                    return;

                #endif

                if (Globals.IsRedditLoopDisabled) {
                    await Task.Delay(random.Next(55000, 120000));
                }
                // Lots of delays needed. We don't wanna spam discord
                await Task.Delay(random.Next(1000, 5000));

                ConsoleMessage.Debug($"Checking reddit page for change. Reddit category: {category}");

                // Scans the subreddit with the appropriate regex pattern
                var grabPageSource = new WebGrab(urlPage);
                var matches = Regex.Matches(grabPageSource.WebResponse, pattern);

                // Count the number of images per reddit post
                var counter = 0;

                foreach (Match match in matches) {
                    var sourcePageImage = match.Groups[1].Value;
                    if (sourcePageImage.EndsWith(".gif") || sourcePageImage.EndsWith(".gifv")
                                                  || sourcePageImage.EndsWith(".png") || sourcePageImage.EndsWith(".jpg")
                                                  || sourcePageImage.Contains("gfycat.com") || sourcePageImage.Contains("//imgur.com")) {

                        // Debug purposes. Checks for console
                        if (verifiedImageUrl == sourcePageImage) {
                            ConsoleMessage.Color($"No new posted images : [Category] {category}", ConsoleColor.Red);
                        } else {
                            ConsoleMessage.Color($"New Image Url {sourcePageImage} : [Category] {category}", ConsoleColor.Blue);
                        }
                        // We only need to check the first post
                        if (counter == 0) {
                            // We pass sourcePageImage to verifiedImageUrl in the end of loop. the second iteration
                            // we check if verifiedImageUrl equals sourcePageImage if it doesn't we post it
                            if (verifiedImageUrl != sourcePageImage) {
                                // Checks if sourcePageImage URL is already in the urlContainer list.
                                if (urlContainer.Contains(sourcePageImage)) {
                                    ConsoleMessage.Color($"Found duplicate string. Duplicate at: {sourcePageImage} | Category: {category}", ConsoleColor.Cyan);
                                    break;
                                }
                                // if discord disconnects halt the task here till discord reconnects. 
                                // We can't post a message while discord isn't connected
                                await TaskHelper.DisconnectedHangTasks(10000);
                                await Globals.DiscordClient.GetGuild(474333807441477661).GetTextChannel(channelId).SendMessageAsync(sourcePageImage);
                                await Task.Delay(500);
                                // I'm a dirty boy ( ͡° ͜ʖ ͡°) 
                                // TODO: Download from users :heart: reactions instead
                                if ((channelId == 582223607497883678 || channelId == 566367044396646402) && !Globals.IsNsfwDownloadDisabled && !sourcePageImage.Contains("//imgur.com")) {
                                    await Task.Delay(1000);
                                    await grabPageSource.ParseImageUrls(sourcePageImage, category);
                                }
                            }
                            // Passes sourcePageImage to verifiedImageUrl to check for duplication
                            verifiedImageUrl = sourcePageImage;
                            urlContainer.Add(verifiedImageUrl);

                            await Task.Delay(random.Next(500, 2000));
                            break;
                        }
                        counter++;
                    }
                }
                // big delay for days xd
                await Task.Delay(random.Next(20000, 30000));
            }
            
        }

        
    }
}
