﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using Discord;
using Discord.Audio;

namespace Bot.Core.Services {
    public class AudioServices {
        private readonly ConcurrentDictionary<ulong, IAudioClient> _connectedChannels =
            new ConcurrentDictionary<ulong, IAudioClient>();

        public IAudioClient AudioClient;
        public AudioOutStream AudioOut;
        public static bool IsMusicActive { get; set; }

        public FileInfo[] AudioFolder() {
            var info = RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
                ? new DirectoryInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "music"))
                : new DirectoryInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"music/test"));

            return info.Exists ? info.GetFiles("*.wav") : null;
        }

        public async Task JoinAudio(IGuild guild, IVoiceChannel target) {
            if (_connectedChannels.TryGetValue(guild.Id, out AudioClient)) return;
            if (target.Guild.Id != guild.Id) return;
            var audioClient = await target.ConnectAsync();

            //await Task.Delay(-1);
            if (_connectedChannels.TryAdd(guild.Id, audioClient))
                ConsoleMessage.Message($"Connected to voice on {guild.Name}.");
        }

        public async Task LeaveAudio(IGuild guild) {
            if (_connectedChannels.TryRemove(guild.Id, out AudioClient)) {
                await AudioClient.StopAsync();
                AudioClient.Dispose();
                IsMusicActive = false;

                ConsoleMessage.Warning($"Is music bot active? {IsMusicActive}");
            }
        }

        public async Task ClearAudio() {
            await AudioOut.FlushAsync();
        }

        public async Task SendAudioAsync(IGuild guild, IMessageChannel channel, string path) {
            if (!File.Exists(path)) {
                await channel.SendMessageAsync("The file does not exist");
                return;
            }

            ConsoleMessage.Warning(path);

            var buffer = 1024;

            if (_connectedChannels.TryGetValue(guild.Id, out AudioClient)) {
                await AudioClient.SetSpeakingAsync(true);

                // This cannot be initiated multiple times as it will keep spamming "Sent Speaking" in console.
                // Find a way to use the PCM Stream once
                using (var ffmpeg = CreateProcess(path)) {
                    using (AudioOut = AudioClient.CreatePCMStream(AudioApplication.Mixed)) {
                        try {
                            await ffmpeg.StandardOutput.BaseStream.CopyToAsync(AudioOut, buffer);
                        }
                        finally {
                            await AudioOut.FlushAsync();
                            ConsoleMessage.Debug("Audio Flushed");
                        }
                    }
                }

                await AudioClient.SetSpeakingAsync(false);
            }
        }

        private Process CreateProcess(string path) {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                return Process.Start(new ProcessStartInfo {
                    FileName = @"E:\Data-Programs\FFMpeg\bin\ffmpeg.exe",
                    Arguments = $"-i \"{path}\" -hide_banner -loglevel error -nostats -ac 2 -f s16le -ar 48000 pipe:1",
                    UseShellExecute = false,
                    RedirectStandardOutput = true
                });

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                return Process.Start(new ProcessStartInfo {
                    FileName = "/bin/bash",
                    Arguments =
                        $"-c \"ffmpeg -i \"{path}\" -hide_banner -loglevel error -nostats -ac 2 -f s16le -ar 48000 pipe:1\"",
                    UseShellExecute = false,
                    RedirectStandardOutput = true
                });
            return null;
        }
    }
}