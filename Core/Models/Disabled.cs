﻿namespace Bot.Core.Models {
    public class Disabled {
        public Disabled(string name, ulong id) {
            Name = name;
            Id = id;
        }

        public string Name { get; set; }
        public ulong Id { get; set; }
    }
}