﻿
namespace Bot.Core.Models {
    public class AdminUsers {
        public AdminUsers(ulong id) {
            Id = id;
        }

        public AdminUsers(string name) {
            Name = name;
        }

        public AdminUsers(string name, ulong id) {
            Name = name;
            Id = id;
        }

        public string Name { get; set; }
        public ulong Id { get; set; }
    }
}