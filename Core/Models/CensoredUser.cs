﻿namespace Bot.Core.Models {
    public class CensoredUsers {
        public CensoredUsers(string name, ulong id) {
            Name = name;
            Id = id;
        }

        public CensoredUsers(ulong id) {
            Id = id;
        }

        public string Name { get; set; }
        public ulong Id { get; set; }
    }
}