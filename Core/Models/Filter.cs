﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bot.Core.Models {
    public class Filter {

        public Filter(string bannedWords) {
            BannedWords = bannedWords;
        }

        public string BannedWords { get; set; }
    }
}
