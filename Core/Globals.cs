﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;

using Bot.Core.Models;
using Bot.Core.Services;
using System.Threading;

namespace Bot.Core {
    public class Globals {
        //Hurray!! Singletons!!
        internal CommandService DiscordCommand { get; set; }
        internal WebScraperServices WebScraper { get; set; }

        internal static DiscordSocketClient DiscordClient { get; set; }
        internal static InsultServices Insulter { get; set; }

        internal static List<CensoredUsers> CensoredList { get; set; }
        internal static List<AdminUsers> AdminFullList { get; set; }
        internal static List<Disabled> SilencedUserList { get; set; }
        internal static List<Filter> FilterList { get; set; }
        internal static List<string> UrlContainer { get; set; }

        internal static Task InsultCooldownTask { get; set; }
        internal static Task OsrsCooldownTask { get; set; }
        internal static Thread RedditThread { get; set; }

        internal static int InsultCooldownTimer { get; set; }
        internal static int OsrsCooldownTimer { get; set; }

        internal static bool IsRedditLoopDisabled { get; set; }
        internal static bool IsNsfwDownloadDisabled { get; set; }
        internal static bool IsDiscordDisconnected { get; set; }

        public static string GetCurrentRandomString {
            get;private set;
        }

        internal static string RandomString(int length) {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++) {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            var randomAssembledStrings = new string(stringChars);
            GetCurrentRandomString = randomAssembledStrings;

            return randomAssembledStrings;
        }
    }
}
