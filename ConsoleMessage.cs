﻿using System;

namespace Bot {
    public class ConsoleMessage {
        /// <summary>
        ///     TODO: Function is repetitive but whatever. I'll fix later. Too lazy
        ///     TODO: When is later? Never...
        /// </summary>
        private static readonly string ConsolePrefix = DateTime.Now.ToString("HH:mm:ss");

        public static void Debug(string message) {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(ConsolePrefix + " DEBUG \t     " + message + " ");
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public static void Error(string message) {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine(ConsolePrefix + " ERROR \t     " + message + " ");
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public static void Warning(string message) {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(ConsolePrefix + " WARNING     " + message + " ");
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public static void Message(string message) {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(ConsolePrefix + " MESSAGE     " + message + " ");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public static void Color(string message, ConsoleColor color) {
            Console.ForegroundColor = color;
            Console.WriteLine(ConsolePrefix + " MESSAGE     " + message + " ");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}